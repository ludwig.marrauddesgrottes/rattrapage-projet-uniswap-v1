import pytest

class Pool:
    def __init__(self, x, y, fee_percent):
        self.x = x
        self.y = y
        self.fee_percent = fee_percent

    def k(self):
        return self.x * self.y

    def fee_factor(self):
        return 1 - self.fee_percent / 100

    def x_spot_price(self):
        return self.y / self.x

    def y_spot_price(self):
        return self.x / self.y

    def swap_x_for_y(self, x_in):
        facteur_frais = self.fee_factor()
        x_in_avec_frais = x_in * facteur_frais
        y_out = self.y - (self.k() / (self.x + x_in_avec_frais))
        self.x += x_in
        self.y -= y_out
        return y_out

    def swap_y_for_x(self, y_in):
        facteur_frais = self.fee_factor()
        y_in_avec_frais = y_in * facteur_frais
        x_out = self.x - (self.k() / (self.y + y_in_avec_frais))
        self.y += y_in
        self.x -= x_out
        return x_out